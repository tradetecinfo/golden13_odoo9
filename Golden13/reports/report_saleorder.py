import time
from openerp.osv import osv
from openerp.report import report_sxw
import logging

_logger = logging.getLogger(__name__)

class sale_quotation_report(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context): 
        super(sale_quotation_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'get_total_crab_qty': self._get_total_crab_qty,
            'get_total_mix_weight_crab': self._get_total_mix_weight_crab,
            'get_total_tere_weight_crab': self._get_total_tere_weight_crab,
            'get_total_net_weight_crab': self._get_total_net_weight_crab,
            'get_total_net_weight_lbs_crab': self._get_total_net_weight_lbs_crab,
            'get_total_rate_price_crab': self._get_total_rate_price_crab,
            'get_total_price_subtotal_crab': self._get_total_price_subtotal_crab,
            'get_total_rclam_qty': self._get_total_rclam_qty,
            'get_total_mix_weight_rclam': self._get_total_mix_weight_rclam,
            'get_total_tere_weight_rclam': self._get_total_tere_weight_rclam,
            'get_total_net_weight_rclam': self._get_total_net_weight_rclam,
            'get_total_rate_price_rclam': self._get_total_rate_price_rclam,
            'get_total_price_subtotal_rclam': self._get_total_price_subtotal_rclam,
            })

    def _get_total_crab_qty(self, lines):
        _logger.info("_get_total_crab_qty" + str(lines))
        total = 0.0
        for line in lines :
            total += line.product_uom_qty or 0.0
        return total

    def _get_total_rclam_qty(self, lines):
        _logger.info("_get_total_rclam_qty" + str(lines))
        total = 0.0
        for line in lines :
            total += line.product_uom_qty or 0.0
        return total

    def _get_total_mix_weight_crab(self, lines):
        _logger.info("_get_total_mix_weight_crab")
        total = 0.0
        for line in lines :
            total += line.mix_weight or 0.0
        return total

    def _get_total_tere_weight_crab(self, lines):
        _logger.info("_get_total_tere_weight_crab")
        total = 0.0
        for line in lines :
            total += line.tere_weight or 0.0
        return total

    def _get_total_net_weight_crab(self, lines):
        _logger.info("_get_total_net_weight_crab")
        total = 0.0
        for line in lines :
            total += line.net_weight or 0.0
        return total

    def _get_total_rate_price_crab(self, lines):
        _logger.info("_get_total_rate_price_crab")
        total = 0.0
        for line in lines :
            total += line.price_unit or 0.0
        return total

    def _get_total_price_subtotal_crab(self, lines):
        _logger.info("_get_total_price_subtotal_crab")
        total = 0.0
        for line in lines :
            total += line.price_subtotal or 0.0
        return total

    def _get_total_net_weight_lbs_crab(self, line):
        _logger.info("_get_total_net_weight_lbs_crab")
        return 123

    def _get_total_mix_weight_rclam(self, lines):
        _logger.info("_get_total_mix_weight_rclam")
        total = 0.0
        for line in lines :
            total += line.mix_weight or 0.0
        return total

    def _get_total_tere_weight_rclam(self, lines):
        _logger.info("_get_total_tere_weight_rclam")
        total = 0.0
        for line in lines :
            total += line.tere_weight or 0.0
        return total

    def _get_total_net_weight_rclam(self, lines):
        _logger.info("_get_total_net_weight_rclam")
        total = 0.0
        for line in lines :
            total += line.net_weight or 0.0
        return total

    def _get_total_rate_price_rclam(self, lines):
        _logger.info("_get_total_rate_price_rclam")
        total = 0.0
        for line in lines :
            total += line.price_unit or 0.0
        return total

    def _get_total_price_subtotal_rclam(self, lines):
        _logger.info("_get_total_price_subtotal_rclam")
        total = 0.0
        for line in lines :
            total += line.price_subtotal or 0.0
        return total

class report_saleOrderDocument(osv.AbstractModel):
    _name = 'report.sale.report_saleorder'
    _inherit = 'report.abstract_report'
    _template = 'sale.report_saleorder'
    _wrapped_report_class = sale_quotation_report