
{
	"name" : "Golden Thirteen Sea Food",
	"version" : "0.1",
	"author" : "hammadHQ",
	"website" : "http://example.com/",
	"category" : "",
	"description": """ 
	Golden thirteen custom module.
		- Date: 10202015 
		- Developer: hammadHQ
	. """,
	"depends" : ['base', 'sale', 'account', 'web'],
	"init_xml" : [ ],
	"demo_xml" : [ ],
	"update_xml" : [],
	"data" : [
	'data/res_company.xml',
	'web_dbrestrict/views/web_dbrestrict.xml', 
	'views/templates.xml',
	'views/sale_view.xml',
	'views/res_partner_view.xml',
	'data/product_data.xml',
	'reports/report_saleorder.xml',
	'security/golden_thirteen_security.xml',
	# 'views/account_invoice_view.xml',
	],
	'qweb' : [
		"static/src/xml/base.xml",
    ],
	'css':[ "static/src/css/golden13.css" ],
	"installable": True
	
}