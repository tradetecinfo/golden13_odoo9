# -*- coding: utf-8 -*-

from openerp import api, fields, models, _
import logging
from lxml import etree

_logger = logging.getLogger(__name__)

def log(value):
	_logger.info("log: " + str(value))

class saleOrder(models.Model):
	_inherit = 'sale.order'

	def get_carton(self, cr, uid, name):
		carton_sizes = self.pool.get("golden13.carton.size")
		carton_ids = carton_sizes.search(cr, uid, [('name', '=', name)])
		if carton_ids:
			_logger.info("get_carton is returning " + str(carton_ids[0]))
			return carton_ids[0]

	def get_default_carton(self, cr=None, uid=None, context=None):

		log(self)
		log(cr)
		log(uid)
		log(context)

		cartons = [
			(0, 0, {'carton_size': self.get_carton(cr, uid, 'AA5'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0, 'price_unit': 0, 'price_subtotal': 0}),
		    (0, 0, {'carton_size': self.get_carton(cr, uid, 'A5'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0, 'price_unit': 0, 'price_subtotal': 0}),
		    (0, 0, {'carton_size': self.get_carton(cr, uid, 'A6'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0, 'price_unit': 0, 'price_subtotal': 0}),
		    (0, 0, {'carton_size': self.get_carton(cr, uid, 'AA1'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0, 'price_unit': 0, 'price_subtotal': 0}),
		    (0, 0, {'carton_size': self.get_carton(cr, uid, 'A1'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0, 'price_unit': 0, 'price_subtotal': 0}),
		    (0, 0, {'carton_size': self.get_carton(cr, uid, 'A7'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0, 'price_unit': 0, 'price_subtotal': 0}),
		    (0, 0, {'carton_size': self.get_carton(cr, uid, 'A3'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0, 'price_unit': 0, 'price_subtotal': 0}),
		    (0, 0, {'carton_size': self.get_carton(cr, uid, 'A8'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0, 'price_unit': 0, 'price_subtotal': 0}),
		]
		
		return cartons


	# @api.model use old api to get all param cr, uid, context
	def default_get(self, cr, uid, fields_list, context=None):

		def get_carton(name):
			carton_ids = carton_sizes.search(cr, uid, [('name', '=', name)])
			if carton_ids:
				_logger.info("get_carton is returning " + str(carton_ids[0]))
				return carton_ids[0]

		res = super(saleOrder, self).default_get(cr, uid, fields_list, context=context)

		carton_sizes = self.pool.get("golden13.carton.size")
		_logger.info("ctxxxxxx" + str(context))
		_logger.info("First Round " + str(res.get("order_line", False)))

		res['order_line'] = [
		    (0, 0, {'carton_size': get_carton('AA5'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0}),
		    (0, 0, {'carton_size': get_carton('A5'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0}),
		    (0, 0, {'carton_size': get_carton('A6'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0}),
		    (0, 0, {'carton_size': get_carton('AA1'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0}),
		    (0, 0, {'carton_size': get_carton('A1'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0}),
		    (0, 0, {'carton_size': get_carton('A7'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0}),
		    (0, 0, {'carton_size': get_carton('A3'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0}),
		    (0, 0, {'carton_size': get_carton('A8'), 'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0, 'net_weight': 0}),]

		res['order_line_razor'] = [(0, 0, {'product_uom_qty': 0, 'mix_weight': 0, 'tere_weight': 0})]
		return res

	# @api.model
	@api.depends('partner_id')
	def get_previous_amount(self):
		previous_amount = 0.0
		log(self.env)
		partner_obj = self.env['res.partner']
		for order in self:
			log("product_id")
			log(order.partner_id.id)
			partner = partner_obj.browse(order.partner_id.id)
			log(partner)
			order.update({
				'previous_amount': partner.previous_amount
				})

	# @api.onchange('order_line')
	# def onchange_price_unit(self):
	# 	_logger.info("//////////////////onchange_price_unit")
	# 	_logger.info("self " + str(self.order_line))
	# 	amount_total = 0
	# 	for line in self.order_line:
	# 		_logger.info("line.price_unit " + str(line.price_subtotal))
	# 		amount_total += line.price_subtotal

	# 	for razor_line in self.order_line_razor:
	# 		_logger.info("rline.price_unit " + str(razor_line.price_subtotal))
	# 		amount_total += razor_line.price_subtotal

	# 	return {'value': {'amount_total': amount_total}}

	def get_previous_amount(self):
		previous_amount = 0.0
		log(self.env)
		partner_obj = self.env['res.partner']
		for order in self:
			log("product_id")
			log(order.partner_id.id)
			partner = partner_obj.browse(order.partner_id.id)
			log(partner)
			order.update({
				'previous_amount': partner.previous_amount
				})


	@api.depends('order_line.price_total','order_line.product_uom_qty', 'order_line_razor.price_total', 'order_line_razor.product_uom_qty')
	def _amount_all(self):
		"""
		Compute the total amounts of the SO.
		"""

		for order in self:
			total_crab = total_razor = 0.0
			log("**********************")
			log(order)
			#crabs
			for line in order.order_line:
				total_crab += line.product_uom_qty * line.price_unit
				log(total_crab)

			for razor_line in order.order_line_razor:
				total_razor += razor_line.product_uom_qty * razor_line.price_unit
			
			_logger.info("total_crab " + str(total_crab))
			_logger.info("total_razor " + str(total_razor))
			order.update({
				'amount_total': total_crab + total_razor,
				'balance': total_crab + total_razor + order.previous_amount,
				})

	# @api.depends('product_id')
	# def onchange_product(self):
	# 	product_name = 'zzz'
	# 	_logger.info(str(self.product_id))
	# 	# on_change="onchange_product(product_id)"
	# 	# val = {'value': {'product_name': product_name}}
	# 	self.product_name = product_name
	# 	return val

	g_weight = fields.Float(string='G/Weight')
	exchange_rate = fields.Float("Exchange Rate @")
	# exchange_rate_amount = fields.Float("Total Amount", compute='_amount_all', track_visibility='always')
	freight_per_kg = fields.Float("Freight Per KG @")
	total_freight_per_kg = fields.Float("Total Freight Per KG @" , compute='_amount_all', track_visibility='always')
	# total_freight_to_box = fields.Float("Total", compute='_amount_all', track_visibility='always')
	cf_mf = fields.Float("C/F & M/F")
	total_cf_mf = fields.Float("Total C/F & M/F")
	rent = fields.Float("Rent")
	total_rent = fields.Float("Total Rent")
	co_labour_expense = fields.Float("Co & Labour Expense")
	total_co_labour_expense = fields.Float("Total Co & Labour Expense")
	box_amount = fields.Float("Box")
	total_box_amount = fields.Float("Total Box")
	net_sale_report = fields.Float("NET SALE REPORT")

	fixed_advance = fields.Float("Fixed Advance")
	per_box_advance = fields.Float("Per Box")
	net_fixed_advance = fields.Float("Fixed Advance")

	balance_bf = fields.Float("Balance B/F")
	paid_jk = fields.Float("Paid")
	balance = fields.Monetary(string='Balance', store=True, readonly=True, compute="_amount_all", track_visibility="always")
	# balance = fields.Float("Balance", compute="_amount_all", track_visibility="always")
	previous_amount = fields.Float("Previous Amount", compute='get_previous_amount',  track_visibility='always')
	# previous_amount = fields.Monetary(string='Previous Balance', store=True, readonly=True, compute='get_previous_amount', track_visibility="always")
	ship_number = fields.Char("Shipping Number")
	order_line = fields.One2many('sale.order.line', 'order_id', string='Order Lines', states={'cancel': [('readonly', True)], 'done': [('readonly', True)]}, copy=True, required=False) #default=lambda self: self.get_default_carton()
	# order_line_crab = fields.One2many("sale.order.line.crab", "order_id", string='Crab Order Lines', states={'cancel': [('readonly', True)], 'done': [('readonly', True)]}, copy=True)
	order_line_razor = fields.One2many("sale.order.line.razor.clam", "order_id", string='Razor Clam Order Lines', states={'cancel': [('readonly', True)], 'done': [('readonly', True)]}, copy=True)

	def onchange_exchange_rate(self, cr, uid, ids, exchange_rate):
		_logger.info('zzzzzz' + str(self))

	def create(self, cr, uid, values, context):
		
		values['state'] = 'sale' 
		res = super(saleOrder, self).create(cr, uid, values, context)
		return res

class saleOrderLine(models.Model):
	"""
	This is for handling crab lines
	"""
	_inherit = 'sale.order.line'

	mix_weight = fields.Float("Mix Weight")
	tere_weight = fields.Float("Tere Weight")
	net_weight = fields.Float("Net Weight")
	exchange_rate = fields.Float("Exchange Rate")
	rent = fields.Float('Exchange Rate')
	carton_size = fields.Many2one("golden13.carton.size", "Size")
	g_weight = fields.Float("G/Weight")
	c_weight = fields.Float("C/Weight")
	product_id = fields.Many2one('product.product', string='Product', domain=[('sale_ok', '=', True)], change_default=True, ondelete='restrict', required=False)

	# def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
	# 	_logger.info("-------------------------------")
	# 	_logger.info("view_id " + str(view_id))
	# 	_logger.info("view_type " + str(view_type))
	# 	_logger.info("context " + str(context))
	# 	_logger.info("toolbar " + str(toolbar))
	# 	_logger.info("submenu " + str(submenu))
	# 	_logger.info("-------------------------------")
		
	# 	res = super(saleOrder, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=submenu)
	# 	doc = etree.XML(res['arch'])
	# 	_logger.info("fields_view_get doc " + str(res['arch']))
	# 	res['arch'] = etree.tostring(doc)
	# 	return res

	def create(self, cr, uid, values, context):
		if context is None:
			context = {}

		product = self.pool.get('product.template')
		product_uom = self.pool.get('product.uom')
		p_uom_ids = product_uom.search(cr, uid, [('name', '=', 'Unit(s)')])
		if p_uom_ids:
			values['product_uom'] = p_uom_ids[0]
			values['name'] = 'Golden thirteen Sea Food'

		p_ids = product.search(cr, uid, [('name', '=', 'Live Crabs')])
		if p_ids:
			values['product_id'] = p_ids[0]	

		_logger.info('values ' + str(values.get('name')))
		res = super(saleOrderLine, self).create(cr, uid, values, context)

		return res

class SaleOrderLine_RazorClam(models.Model):
	"""docstring for SaleOrderLine_RazorClam"""
	_inherit="sale.order.line"
	_name="sale.order.line.razor.clam"

	product_id = fields.Many2one('product.product', string='Product', domain=[('sale_ok', '=', True)], change_default=True, ondelete='restrict', required=False)
	def create(self, cr, uid, values, context):
		if context is None:
			context = {}

		product = self.pool.get('product.template')
		product_uom = self.pool.get('product.uom')
		p_uom_ids = product_uom.search(cr, uid, [('name', '=', 'Unit(s)')])
		if p_uom_ids:
			values['product_uom'] = p_uom_ids[0]
			values['name'] = 'Golden thirteen Sea Food'

		p_ids = product.search(cr, uid, [('name', '=', 'Razor Clam')])
		if p_ids:
			values['product_id'] = p_ids[0]	

		_logger.info('values ' + str(values.get('name')))
		res = super(SaleOrderLine_RazorClam, self).create(cr, uid, values, context)

		return res

SaleOrderLine_RazorClam()

class SaleOrderLine_Crab(models.Model):
	"""docstring for SaleOrderLine_RazorClam"""

	_inherit="sale.order.line"
	_name="sale.order.line.crab"
		

	def create(self, cr, uid, values, context=None):
		product = self.pool.get('product.template')
		p_ids = product.search(cr, uid, [('name', '=', 'Live Crabs')])
		if p_ids:
			values['product_id'] = p_ids[0]	

		res = super(SaleOrderLine_Crab, self).create(cr, uid, values, context)
		return res
SaleOrderLine_Crab()

class cartonSize(models.Model):
	_name = "golden13.carton.size"

	name = fields.Char('Size')
	weight = fields.Float('Weight')
	description = fields.Text('Description')

class procurement_order(models.Model):
	"""docstring for procurement_order"""
	_inherit="procurement.order"

	def create(self, cr, uid, values, context=None):
		_logger.info('str' + str(values))
		# dont create procurement
		# res = super(procurement_order, self).create(cr, uid, values, context)
		return 