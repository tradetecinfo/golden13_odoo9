
from openerp import api, fields, models, _

class res_partner(models.Model):
	"""docstring for res_partner"""
	_inherit = "res.partner"

	previous_amount = fields.Float('Previous Amount')	