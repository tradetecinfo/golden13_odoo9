# -*- coding: utf-8 -*-

from openerp import api, fields, models, _
import logging

_logger = logging.getLogger(__name__)

class AccountInvoice(models.Model):
	_inherit="account.invoice"
	
	exchange_rate = fields.Float("Exchange Rate @")
	exchange_rate_amount = fields.Float("Total Amount", compute='_amount_all', track_visibility='always')
	freight_per_kg = fields.Float("Freight Per KG @")
	total_freight_per_kg = fields.Float("Total Freight Per KG @" , compute='_amount_all', track_visibility='always')
	total_freight_to_box = fields.Float("Total", compute='_amount_all', track_visibility='always')
	cf_mf = fields.Float("C/F & M/F")
	total_cf_mf = fields.Float("Total C/F & M/F")
	rent = fields.Float("Rent")
	total_rent = fields.Float("Total Rent")
	co_labour_expense = fields.Float("Co & Labour Expense")
	total_co_labour_expense = fields.Float("Total Co & Labour Expense")
	box_amount = fields.Float("Box")
	total_box_amount = fields.Float("Total Box")
	net_sale_report = fields.Float("NET SALE REPORT")
			
	
