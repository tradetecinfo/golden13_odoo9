
from openerp import models, fields

class saleOrder(models.Model):
	""" for sale_order """
	_inherit = 'sale.order'

	exchange_rate = fields.Float("Exchange Rate @")
	exchange_rate_amount = fields.Float("Total Amount")
	freight_per_kg = fields.Float("Freight Per KG @")
	total_freight_per_kg = fields.Float("Total Freight Per KG @")
	total_freight_to_box = fields.Float("Total")
	cf_mf = fields.Float("C/F & M/F")
	total_cf_mf = fields.Float("Total C/F & M/F")
	rent = fields.Float("Rent")
	total_rent = fields.Float("Total Rent")
	co_labour_expense = fields.Float("Co & Labour Expense")
	total_co_labour_expense = fields.Float("Total Co & Labour Expense")
	box_amount = fields.Float("Box")
	total_box_amount = fields.Float("Total Box")

	net_sale_report = fields.Float("NET SALE REPORT")

class saleOrderLine(models.Model):
	_inherit = 'sale.order.line'	
		
	mix_weight = fields.Float("Mix Weight")
	tere_weight = fields.Float("Tere Weight")
	net_weight = fields.Float("Net Weight")
	exchange_rate = fields.Float("Exchange Rate")
	rent = fields.Float('Exchange Rate')
	carton_size = fields.Many2one("golden13.carton.size", "Size")

class cartonSize(models.Model):
	_name = "golden13.carton.size"

	name = fields.Char('Size')
	weight = fields.Float('Weight')
	description = fields.Text('Description')